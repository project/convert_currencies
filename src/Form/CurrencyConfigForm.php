<?php

namespace Drupal\convert_currencies\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\convert_currencies\FixerConverterService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Currency configuration form.
 */
class CurrencyConfigForm extends ConfigFormBase {

  /**
   * The currency conversion service.
   *
   * @var \Drupal\convert_currencies\FixerConverterService
   */
  protected $converter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    FixerConverterService $converter,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configFactory);
    $this->converter = $converter;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('convert_currencies.converter'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'convert_currencies.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'currency_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('convert_currencies.settings');

    $hasKey = !empty($config->get('fixer_api'));
    $form['fixer_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fixer API Key'),
      // '#default_value' => $config->get('fixer_api'),
      '#placeholder' => $hasKey ? str_repeat('*', 32) : 'Enter API key here',
      '#description' => $hasKey
        ? 'Leave empty to skip field update'
        : 'Add Fixer API key',
    ];

    // Load all currencies.
    $currencies = $this->entityTypeManager
      ->getStorage('currency')
      ->loadMultiple();

    $currency_options = [];
    foreach ($currencies as $id => $currency) {
      $currency_options[$id] = $currency->label();
    }

    $form['base_currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Base Currency'),
      '#default_value' => $config->get('base_currency') ?? 'eur',
      '#options' => $currency_options,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('convert_currencies.settings');
    if ($form_state->hasValue('fixer_api')
      && !empty($form_state->getValue('fixer_api'))) {
      $config->set('fixer_api', $form_state->getValue('fixer_api'));
    }
    $config->set('base_currency', $form_state->getValue('base_currency'));
    $config->save();

    // Trigger a currency update.
    $this->converter->updateCurrencies();

    parent::submitForm($form, $form_state);
  }

}
