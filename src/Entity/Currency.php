<?php

namespace Drupal\convert_currencies\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Currency entity.
 *
 * @ContentEntityType(
 *   id = "currency",
 *   label = @Translation("Currency"),
 *   translatable = FALSE,
 *   handlers = {
 *     "list_builder" = "Drupal\convert_currencies\Controller\CurrencyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\convert_currencies\Form\CurrencyBaseForm",
 *       "edit" = "Drupal\convert_currencies\Form\CurrencyBaseForm",
 *       "delete" = "Drupal\convert_currencies\Form\CurrencyDeleteForm",
 *     }
 *   },
 *   base_table = "currency",
 *   admin_permission = "administer currencies",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "baseRate" = "baseRate"
 *   },
 *   links = {
 *     "collection" = "/admin/config/fixer_currencies",
 *     "add-form" = "/admin/config/fixer_currencies/add",
 *     "edit-form" = "/admin/config/fixer_currencies/{currency}/edit",
 *     "delete-form" = "/admin/config/fixer_currencies/{currency}/delete",
 *   }
 * )
 */
class Currency extends ContentEntityBase {

  /**
   * The currency ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The currency label.
   *
   * @var string
   */
  protected $label;

  /**
   * The base currency rate.
   *
   * @var float
   */
  protected $baseRate;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseRate() {
    return $this->baseRate;
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseRate($rate) {
    $this->set('baseRate', $rate);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('ID'))
      ->setDescription(t('The currency ID.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The currency label.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['baseRate'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Base Currency Rate'))
      ->setDescription(t('The base conversion rate of the currency.'))
      ->setDefaultValue(0)
      ->setSettings([
        'scale' => 6,
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setTranslatable(FALSE);

    return $fields;
  }

}
