# Currency Converter Module

The Currency Converter module is a versatile tool for storing and converting
currencies. It provides a predefined list of currencies that can be extended
through the configuration page. The module updates exchange rates for all stored
currencies either via cron or when a currency provider configuration is updated.
It also offers a convenient service for converting prices.

## Why Fixer API?

The module currently implements the [Fixer API](https://fixer.io/documentation)
as the default currency provider. The Fixer API is a straightforward and
user-friendly option, offering a free plan that is more than sufficient for
development purposes. However, the module is designed to support other APIs in
the future, and contributions to add new providers are welcome.

# How to Use

Follow these steps to use the Currency Converter module:

1. Install the module in your Drupal project.
2. Access the currency configuration page at `/admin/config/fixer_currencies`
and set up the currencies you need.
3. Obtain an access key from the Fixer API dashboard. It is recommended to store
the access key in your `settings.php` or `.env` file to avoid storing it in the
Git history. Use the following code snippet in your `settings.php` to set the
access key:
```php
$config['convert_currencies.settings']['fixer_api'] = 'YOUR-FIXER-API-KEY';
```
4. Add the desired currencies by specifying their
[ISO codes](https://en.wikipedia.org/wiki/List_of_circulating_currencies) as ID.
Note that in the future, the process of adding currencies will be streamlined.
5. Utilize the currency conversion service in your code. There are two ways to
do this:
a. Procedural style (use in hooks):
```php
$container = \Drupal::getContainer();
$currencyConverter = $container->get('convert_currencies.converter');
$example = $currencyConverter->convert(100.98, 'USD', 'CAD');
```
b. Object-oriented style (example from CurrencyBlock.php):
```php
use Drupal\convert_currencies\FixerConverterService;
$container->get('convert_currencies.converter'),
$value = $this->converter->convert($amount, $fromCurrency, $toCurrency);
```
Please refer to the Example Block module for further guidance.

# Notes

- Please note that with the free plan of the Fixer API, only 'EUR' can be used
as the base currency. Consequently, it cannot be deleted. If you have a
different base currency, you won't be able to delete it either, similar to
'EUR'.

# Points of improvement

To enhance the Currency Converter module, the following points are considered
for future development:
- Update the code to leverage PHP 8+ features such as type casting.
- Test compatibility with Drupal 10 and update the code if necessary.
- Enable automatic testing.
- Enhance the currency configuration process by providing a list of all
currencies with an 'enabled' state instead of manually typing currency codes in
the ID field. Additionally, update the ID field to only accept lowercase
letters.
- Design the code to be extendable, enabling the addition of new currency rate
providers. Update the configuration schema accordingly and incorporate new
providers.
- Expand currency information to include not only labels, machine names, and
rates but also full names (e.g., "Georgian Lari") and currency symbols
(e.g., "$"). Retrieve this data using a service.
- Ensure support for translatability, allowing the module to handle different
languages effectively.
- Implement clearing of the block cache after currency updates to provide
accurate and up-to-date conversions.
- Create a comprehensive help page to guide users on installing, configuring,
and utilizing the module's features effectively.
