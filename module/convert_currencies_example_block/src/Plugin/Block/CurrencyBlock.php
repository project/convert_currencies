<?php

namespace Drupal\convert_currencies_example_block\Plugin\Block;

use Drupal\convert_currencies\FixerConverterService;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a currency example block.
 *
 * @Block(
 *   id = "convert_currencies_custom_block",
 *   admin_label = @Translation("Currency Block"),
 *   category = @Translation("Currency")
 * )
 */
class CurrencyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The currency conversion service.
   *
   * @var \Drupal\convert_currencies\FixerConverterService
   */
  protected $converter;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new CurrencyBlock object.
   *
   * @param array $configuration
   *   The block configuration.
   * @param string $plugin_id
   *   The block plugin ID.
   * @param mixed $plugin_definition
   *   The block plugin definition.
   * @param \Drupal\convert_currencies\FixerConverterService $converter
   *   The currency conversion service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FixerConverterService $converter,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->converter = $converter;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('convert_currencies.converter'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'amount' => 100,
      'from_currency' => 'USD',
      'to_currency' => 'RUB',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['amount'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amount'),
      '#default_value' => $config['amount'],
    ];

    $form['from_currency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Currency'),
      '#default_value' => $config['from_currency'],
    ];

    $form['to_currency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To Currency'),
      '#default_value' => $config['to_currency'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['amount'] = $values['amount'];
    $this->configuration['from_currency'] = $values['from_currency'];
    $this->configuration['to_currency'] = $values['to_currency'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get block values.
    $config = $this->getConfiguration();
    $amount = $config['amount'];
    $fromCurrency = $config['from_currency'];
    $toCurrency = $config['to_currency'];

    // Perform the currency conversion using the injected converter service.
    $value = $this->converter->convert($amount, $fromCurrency, $toCurrency);

    // Get the base currency from the injected config factory.
    $settings = $this->configFactory->get('convert_currencies.settings');
    $baseCurrency = strtoupper($settings->get('base_currency'));

    $build['#markup'] = $this->t(
      '@amount of @fromCurrency in @toCurrency is @value @baseCurrency',
      [
        '@amount' => $amount,
        '@fromCurrency' => $fromCurrency,
        '@toCurrency' => $toCurrency,
        '@value' => $value,
        '@baseCurrency' => $baseCurrency,
      ]
    );

    return $build;
  }

}
