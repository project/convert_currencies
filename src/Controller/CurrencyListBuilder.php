<?php

namespace Drupal\convert_currencies\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of currency entities.
 */
class CurrencyListBuilder extends EntityListBuilder {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeInterface $entityType,
    EntityStorageInterface $storage,
    FormBuilderInterface $formBuilder,
  ) {
    parent::__construct($entityType, $storage);

    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type,
  ) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'convert_currencies';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Currency name');
    $header['id'] = $this->t('Machine Name');
    $header['baseRate'] = $this->t('Base rate');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['baseRate'] = $entity->getBaseRate();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Create an instance of the CurrencyBaseSelectForm. It contains
    // configuration fields.
    $form = $this->formBuilder->getForm(
      'Drupal\convert_currencies\Form\CurrencyConfigForm'
    );

    // Add the form to the $build array.
    $build['currency_config'] = $form;

    return $build;
  }

}
