<?php

namespace Drupal\convert_currencies\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Currency entity.
 */
class CurrencyDeleteForm extends ContentEntityDeleteForm {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the currency %label?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.currency.list');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Check if currency is safe to remove.
    if ($this->isBaseCurrencyCandidate()) {
      // Set a message .
      $this->messenger()->addWarning($this->t('The %name can not be deleted now.', [
        '%name' => $this->entity->label(),
      ]));
    }
    else {
      $entity = $this->getEntity();
      $entity->delete();

      $this->messenger()->addStatus($this->t('Currency %label has been deleted.', ['%label' => $entity->label()]));
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Checks if currency is possibly base.
   *
   * @return bool
   *   The currency is currently a base currency, or a default Fixer's currency.
   */
  protected function isBaseCurrencyCandidate() {
    $id = $this->entity->getId();

    // Always keep the Euro for reference, as it's the only available option
    // for free accounts.
    if ($id === 'eur') {
      return TRUE;
    }

    // Get the base currency from the injected config factory.
    $settings = $this->configFactory->get('convert_currencies.settings');
    $baseCurrency = $settings->get('base_currency');

    if ($id === $baseCurrency) {
      return TRUE;
    }

    return FALSE;
  }

}
