<?php

namespace Drupal\convert_currencies;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Converter service that uses Fixer API.
 */
class FixerConverterService {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity query object.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    LoggerInterface $logger,
    MessengerInterface $messenger,
    ClientInterface $httpClient
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->get('currency'), // TODO: CURRENCY?!
      $container->get('config.factory'),
      $container->get('logger.channel.default'),
      $container->get('messenger'),
      $container->get('core.http_client'),
    );
  }

  /**
   * Update currency rates.
   */
  public function updateCurrencies() {
    // Get config values.
    $config = $this->configFactory->get('convert_currencies.settings');
    $accessKey = $config->get('fixer_api');
    $baseCurrency = $config->get('base_currency') ?? 'eur';

    // Get currencies to update.
    $ids = $this->entityTypeManager
      ->getStorage('currency')
      ->getQuery()
      ->execute();

    $storage = $this->entityTypeManager->getStorage('currency');
    $currencyList = $storage->loadMultiple($ids);

    // Prepare string for the request.
    $symbols = strtoupper(implode(',', $ids));

    // Set the API endpoint URL with query parameters.
    $url = "http://data.fixer.io/api/latest?access_key={$accessKey}&base={$baseCurrency}&symbols={$symbols}";

    try {
      // Send the GET request to the API endpoint.
      $response = $this->httpClient->get($url);

      // Check the response status code.
      if ($response->getStatusCode() === 200) {
        // Get the response body.
        $content = $response->getBody()->getContents();

        // Process the response.
        $data = json_decode($content, TRUE);

        if ($data['success'] === TRUE && isset($data['rates'])) {
          // Update entities.
          foreach ($data['rates'] as $currencyKey => $rate) {
            $currency = $currencyList[strtolower($currencyKey)];
            $currency->setBaseRate($rate);
            $currency->save();
          }
        }
        else {
          $this->setErrors("Error code: {$data['error']['code']}, type: {$data['error']['type']}");
        }
      }
      else {
        $this->setErrors('Error: Unable to retrieve exchange rate data.');
      }
    }
    catch (\Exception $e) {
      $this->setErrors($e->getMessage());
    }
  }

  /**
   * Converts $value of $fromCurrency to $toCurrency.
   *
   * @param float $value
   *   Value to convert.
   * @param string $fromCurrency
   *   Initial currency.
   * @param string $toCurrency
   *   Resulting currency.
   *
   * @return float
   *   Value in $toCurrency.
   */
  public function convert(float $value, string $fromCurrency, string $toCurrency): float {
    // Get currencies.
    $storage = $this->entityTypeManager->getStorage('currency');
    $fromCurrency = $storage->load(strtolower($fromCurrency));
    $toCurrency = $storage->load(strtolower($toCurrency));
    $fromCurrency = !empty($fromCurrency) ? $fromCurrency->getBaseRate() : 0;
    $toCurrency = !empty($toCurrency) ? $toCurrency->getBaseRate() : 0;

    if ($fromCurrency == 0 || $toCurrency == 0) {
      return 0;
    }

    // Convert $fromCurrency to amount in $baseCurrency.
    $baseCurrencyAmount = $value / $fromCurrency;

    // Convert $baseCurrencyAmount to $toCurrency.
    $result = $baseCurrencyAmount * $toCurrency;

    return $result;
  }

  /**
   * Set message and write to log.
   *
   * @param string $messageText
   *   Message text.
   */
  private function setErrors(string $messageText) {
    $this->messenger->addError($messageText);
    $this->logger->error($messageText);
  }

}
